# Gohello

A go hello world example

## Golang environment

```bash
# add this in your bashrc
export GOROOT=$HOME/.local/go
export GOPATH=$HOME/sources/golang
export GOBIN=$HOME/sources/golang/bin
source ~/.bashrc
```

## Clone, install, build and run

```bash
git clone https://gitlab.com/elnox/gohello.git && cd ./gohello
./latest
make && make run
./build/gohello
```
